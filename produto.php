<?php 
	
	include "banco.php";
	include "ajudantes.php";

	$cadastro_produto = true;
	$cadastro_tproduto = false;
	$cadastro_imposto = false;
	$vendas = false;
	$sobre = false;
	$tem_erros = false;
	$erros_validacao = array();

	if (tem_post()){
	
		$produtos = array();

		if (isset($_POST['nome']) && strlen($_POST['nome']) > 0){
			$produtos['nome'] = $_POST['nome'];	
		}
		else{
			$tem_erros = true;
			$erros_validacao['valor'] = 'O valor é obrigatório!';
		}

		if (isset($_POST['tp'])){
			$produtos['tp'] = $_POST['tp'];
		}

		if (isset($_POST['preco']) && $_POST['preco'] > 0){
			$produtos['preco'] = $_POST['preco'];
		}

		if (! $tem_erros){
			cadastrar_produto($dbconn, $produtos);
			header ('Location: vendas.php');
			die();
		}
	}

	$tipos_produtos = buscar_tp($dbconn);

	include "template.php"
?>