<?php
	session_start(); 
	include "banco.php";
	include "ajudantes.php";

	$cadastro_produto = false;
	$cadastro_tproduto = false;
	$cadastro_imposto = false;
	$sobre = false;
	$vendas = true;
	$tabela = false;

	if (isset($_POST['produto']) && strlen($_POST['produto']) > 0 && isset($_POST['quantidade']) && strlen($_POST['quantidade']) > 0 ){
		$lista_produto = array();
		$produto = buscar_produto($dbconn, $_POST['produto']);
		$imposto = buscar_imposto($dbconn, $produto['tp_id']);

		$lista_produto['produto'] = $produto['nome'];
		$lista_produto['quantidade'] = $_POST['quantidade'];
		$lista_produto['imposto'] = ($imposto['valor_percentual']/100) * ($produto['preco']);
		$lista_produto['preco_unidade'] = $produto['preco'];
		$lista_produto['preco'] = ($imposto['valor_percentual']/100) * ($produto['preco'] * $_POST['quantidade']) + ($produto['preco'] * $_POST['quantidade']);

		gravar_venda($dbconn, $lista_produto);
		
	}

	$lista_produtos = buscar_vendas($dbconn);
	if (count($lista_produtos) > 0){
		$tabela = true;
	}
	$tipos_produtos = buscar_produtos($dbconn);

	$totals = array();
	$total_imposto = 0;
	$total_compras = 0;
	$totals['total_compras'] = 0;
	$totals['total_imposto'] = 0;
	foreach ($lista_produtos as $produto){
		$totals['total_compras'] += $produto['preco'];
		$totals['total_imposto'] += $produto['imposto']; 
	}

	include "template.php"
?>