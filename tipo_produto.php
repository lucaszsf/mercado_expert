<?php 
	include "banco.php";
	include "ajudantes.php";
	
	$cadastro_produto = false;
	$cadastro_tproduto = true;
	$cadastro_imposto = false;
	$vendas = false;
	$sobre = false;
	$tem_erros = false;
	$erros_validacao = array();

	if (tem_post()){
	
		$tp = array();

		if (isset($_POST['nome']) && strlen($_POST['nome']) > 0){
			$tp['nome'] = $_POST['nome'];	
		}
		else{
			$tem_erros = true;
			$erros_validacao['nome'] = 'O nome é obrigatório!';
		}

		if (! $tem_erros){
			cadastrar_tp($dbconn, $tp);
			header ('Location: vendas.php');
			die();
		}
	}

	include "template.php"
?>