<?php
  $dbconn = pg_connect("host=localhost port=5432 dbname=mercado 
                        user=postgres password=postgre")
            or die('Não foi possível conectar: ' . pg_last_error());

	function cadastrar_tp($dbconn, $tp){
		$query = "INSERT INTO tipo_produto (nome) VALUES
		(
			'{$tp['nome']}'
    )";

    pg_query($dbconn, $query);
  }

	function cadastrar_imposto($dbconn, $imposto){
		$query = "INSERT INTO imposto (valor_percentual, tp_id) VALUES
		(
			{$imposto['valor']},
			{$imposto['tp']}
    )";
    $result = pg_query($dbconn, $query);
  }

  function cadastrar_produto($dbconn, $produtos){
    $query = "INSERT INTO produtos (nome, tp_id, preco) VALUES
    (
      '{$produtos['nome']}',
      {$produtos['tp']},
      {$produtos['preco']}
    )";
    $result = pg_query($dbconn, $query);
  }

	function buscar_tp($dbconn){
		$query = "SELECT * FROM tipo_produto";
		$result = pg_query($dbconn, $query);
		$tps = array();

		while ($tp = pg_fetch_assoc($result)){
		  $tps[] = $tp;
    }

		return $tps;
  }

  function buscar_tp_imposto($dbconn){
    $query = "SELECT * FROM tipo_produto WHERE id NOT IN (SELECT tp_id FROM imposto)";
    $result = pg_query($dbconn, $query);
    $tps = array();

    while ($tp = pg_fetch_assoc($result)){
      $tps[] = $tp;
    }

    return $tps;
  }

  function buscar_produtos($dbconn){
    $query = "SELECT * FROM produtos";
    $result = pg_query($dbconn, $query);
    $produtos = array();

    while ($tp = pg_fetch_assoc($result)){
      $produtos[] = $tp;
    }

    return $produtos;
  }

  function buscar_produto($dbconn, $id){
    $query = "SELECT * FROM produtos WHERE id = " . $id;
    $result = pg_query($dbconn, $query);
    return pg_fetch_assoc($result);
  }

  function buscar_imposto($dbconn, $id){
    $query = "SELECT * FROM imposto WHERE tp_id = " . $id;
    $result = pg_query($dbconn, $query);
    return pg_fetch_assoc($result);
  }

  function gravar_venda($dbconn, $venda){
     $query = "INSERT INTO vendas (produto, quantidade, imposto, preco, preco_unidade) VALUES
    (
      '{$venda['produto']}',
      {$venda['quantidade']},
      {$venda['imposto']},
      {$venda['preco']},
      {$venda['preco_unidade']}
    )";
    $result = pg_query($dbconn, $query);
  }

  function buscar_vendas($dbconn){
    $query = "SELECT * FROM vendas";
    $result = pg_query($dbconn, $query);
    $vendas = array();

    while ($venda = pg_fetch_assoc($result)){
      $vendas[] = $venda;
    }

    return $vendas;
  }

  function remover_venda($dbconn, $id){
    $sqlRemover = "DELETE FROM vendas WHERE id = {$id}";
    pg_query($dbconn, $sqlRemover);
  }

  function remover_vendas($dbconn){
    $sqlRemover = "DELETE FROM vendas";
    pg_query($dbconn, $sqlRemover);
  }

?>