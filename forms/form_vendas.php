<form method="POST">
	<div class="div_caixa">
		<div class="Medium-6 columns">
			<label> Produto
				<select class="chosen" name="produto">
					<?php 
						foreach ($tipos_produtos as &$tp) {
							echo "<option value={$tp['id']}>{$tp['nome']}</option>";
						}
					?>
				</select>
			</label>
			<label> Quant.
				<input type="number" name="quantidade" placeholder="Quantidade">
			</label>
		</div>
	</div>
	<div class="input-group-button">
		<input type="submit" class="button " value="Adicionar">
	</div>
</form>
