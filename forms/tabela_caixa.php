<br> 
<table>
	<tr>
		<th>Produto</th>
		<th>Quantidade</th>
		<th>R$ Imposto pago por uni. </th>
		<th>R$ Preço unid.</th>
		<th>R$ Preço total</th>
		<th>Opções</th>
	</tr>
	<?php foreach ($lista_produtos as $produto) : ?> 
		<tr>
			<td><?php echo $produto['produto']; ?> </td>
			<td><?php echo $produto['quantidade']; ?> </td>
			<td><?php echo round($produto['imposto'], 2); ?> x <?php echo $produto['quantidade']; ?> </td>
			<td><?php echo round($produto['preco_unidade'], 2); ?> </td>
			<td><?php echo round($produto['preco'], 2); ?> </td>
			<td>
				<a class="alert button" href="remover.php?id=<?php echo $produto['id']; ?>"> Remover </a>
			</td>
		</tr>
	<?php endforeach; ?>
</table>

<div class="row">
	<h4>Total da compra: <?php echo $totals['total_compras']; ?>R$</h4>
	<h4>Total de imposto na compra: <?php echo $totals['total_imposto']; ?>R$</h4>
</div>

<div class="button-group">
	<a class="warning button" href="remover_vendas.php">Finalizar Compra</a>
</div>