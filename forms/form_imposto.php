<form method="POST">
	<div class="row">
		<div class="Medium-6 columns">
			<label> Valor em percentual
				<input type="number" name="valor" placeholder="Valor porcentual do tipo de produto">
			</label>
		</div>
		<div class="Medium-6 columns">
			<label> Tipo do Produto <br>
				<select class="chosen" name="tp">
					<?php 
						foreach ($tipos_produtos as &$tp) {
							echo "<option value={$tp['id']}>{$tp['nome']}</option>";
						}
					?>
				</select>
			</label>
		</div>
	</div>
	<div class="input-group-button">
		<br>
		<input type="submit" class="button " value="Cadastrar">
	</div>
</form>
