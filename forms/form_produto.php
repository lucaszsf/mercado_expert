<form method="POST">
	
	<div class="row">
		<div class="Medium-6 columns">
			<label> Nome
				<input type="text" name="nome" placeholder="Nome do produto">
			</label>
		</div>
		<div class="Medium-6 columns">
			<label> Tipo do Produto <br>
				<select class="chosen" name="tp">
					<?php 
						foreach ($tipos_produtos as &$tp) {
							echo "<option value={$tp['id']}>{$tp['nome']}</option>";
						}
					?>
				</select>
			</label>
		</div>

		<div class="Medium-6 columns">
		<br>
			<label> Preço
				<input type="number" name="preco" step="any" placeholder="Preço do Produto">
			</label>
		</div>
	</div>
	<div class="input-group-button">
		<input type="submit" class="button" value="Cadastrar">
	</div>
</form>
