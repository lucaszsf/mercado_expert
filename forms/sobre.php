<p>
	Desafio:
	<br><br>
	Pagamos tributos em todos os serviços e produtos que consumimos, estes tributos são diferentes, dependendo do tipo de produto comercializado. Recentemente algumas iniciativas estão sendo implantadas para esclarecer ao consumidor quanto realmente custa o produto e quanto do valor pago são impostos.
	<br><br>
	Desenvolva um programa para um mercado que permita o cadastro dos produtos, dos tipos de cada produto, dos valores percentuais de imposto dos tipos de produtos, a tela de venda, onde será informado os produtos e quantidades adquiridos, o sistema deve apresentar o valor de cada item multiplicado pela quantidade adquirida e a quantidade pago de imposto em cada item, um totalizador do valor da compra e um totalizador do valor dos impostos.
	<br><br>
	O sistema deve ser desenvolvido utilizando as seguintes tecnologias:
	<ul>
		<li>PHP 5</li>
		<li>Banco de dados PostgreSQL versão 9.4   </li>
	</ul>       
	Quaisquer dúvidas sobre a especificação do programa a ser desenvolvido, podem ser questionados por email   
	<br><br>
	Deverão ser enviados para análise:  
	<ul>
		<li>O sistema no formato ZIP para realizar o deploy no servidor de aplicação.   </li>
		<li> O banco de dados utilizado.</li>
		<li>Os detalhes de configuração do banco de dados, como o nome de usuário e senha, o nome do alias do banco de dados e 	informações adicionais para validarmos o aplicativo.
		</li>
	</ul>       
</p>