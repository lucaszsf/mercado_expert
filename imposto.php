<?php 
	include "banco.php";
	include "ajudantes.php";

	$cadastro_produto = false;
	$cadastro_tproduto = false;
	$cadastro_imposto = true;
	$vendas = false;
	$sobre = false;
	$tem_erros = false;
	$erros_validacao = array();

	if (tem_post()){
	
		$imposto = array();

		if (isset($_POST['valor']) && strlen($_POST['valor']) > 0){
			$imposto['valor'] = $_POST['valor'];	
		}
		else{
			$tem_erros = true;
			$erros_validacao['valor'] = 'O valor é obrigatório!';
		}

		if (isset($_POST['tp'])){
			$imposto['tp'] = $_POST['tp'];
		}

		if (! $tem_erros){
			cadastrar_imposto($dbconn, $imposto);
			header ('Location: vendas.php');
			die();
		}
	}

	$tipos_produtos = buscar_tp_imposto($dbconn);

	include "template.php";
?>