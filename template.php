<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Vendas</title>
		<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="static/js/chosen.jquery.js"></script>
		<script type="text/javascript" src="static/js/chosen.jquery.min.js"></script>
		<link rel="stylesheet" href="static/css/style.css?ver=<?php echo filemtime('static/css/style.css');?>">
		<link rel="stylesheet" href="static/css/foundation.css">
		<link rel="stylesheet" href="static/css/app.css">
		<link rel="stylesheet" href="static/css/chosen.min.css">
		
		<script type="text/javascript">
			$(document).ready(function(e){
				$(".chosen").chosen({
				});
			});

		</script>

	</head>
	<body>
		<div class="top-bar">
		  <div class="top-bar-left">
		    <ul class="dropdown menu" data-dropdown-menu>
		      <li class="menu-text"><a href="vendas.php">Mercadinho Expert</a></li>
		      <li><a href="vendas.php">Vender</a></li>
		      <li>
		        <a href="#">Cadastros</a>
		        <ul class="menu vertical">
		          <li><a href="produto.php">Produtos</a></li>
  				<li><a href="tipo_produto.php">Tipo de Produtos</a></li>
  				<li><a href="imposto.php">Impostos</a></li>
		        </ul>
		      </li>
		      <li><a href="sobre.php">Sobre a Vaga</a></li>
		    </ul>
		  </div>
		</div>

		<div class="auxiliar-body">
			<?php if ($vendas): ?>
				<h1 class="center">Caixa</h1>
				<?php include "forms/form_vendas.php"; ?>
				<?php if ($tabela): ?>
					<?php include "forms/tabela_caixa.php"; ?>
				<?php endif; ?>
			<?php endif; ?>
		
			<?php if ($cadastro_produto): ?>
				<h1 class="center">Cadastro de Produtos</h1>
				<?php include "forms/form_produto.php"; ?>
			<?php endif; ?>
		
			<?php if ($cadastro_tproduto): ?>
				<h1 class="center">Cadastro de Tipo de Produtos</h1>
				<?php include "forms/form_tproduto.php"; ?>
			<?php endif; ?>
		
			<?php if ($cadastro_imposto): ?>
				<h1 class="center">Cadastro de Imposto</h1>
				<?php include "forms/form_imposto.php"; ?>
			<?php endif; ?>

			<?php if ($sobre): ?>
				<h1 class="center">Sobre a vaga</h1>
				<?php include "forms/sobre.php"; ?>
			<?php endif; ?>
		</div>

	 	
	    <script src="static/js/vendor/what-input.js"></script>
	    <script src="static/js/vendor/foundation.js"></script>
	    <script src="static/js/app.js"></script>
	</body>
</html>